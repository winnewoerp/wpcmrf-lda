<?php
/*
Plugin Name: CiviMRF Local Database Addon
Plugin URI: https://www.stadtkreation.de/en/wordpress-plugins-and-themes/
Description: Use this plugin in a WordPress network installation to add database connections to your local database. WPCMRF plugin is not doing this automatically. 
Version: 0.1
Author: STADTKREATION
Text Domain: wpcmrf-lda
Domain Path: /languages/
Author URI: https://www.stadtkreation.de/en/about-us/
*/

global $plugin_name;
$plugin_name = esc_html__('CiviMRF Local Database Addon','wpcmrf-lda');
esc_html__('https://www.stadtkreation.de/en/wordpress-plugins-and-themes/','wpcmrf-lda');
esc_html__('https://www.stadtkreation.de/en/about-us/','wpcmrf-lda');

function has_wpcmrf_check() {
    if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'connector-civicrm-mcrestface/wpcmrf.php' ) ) {
        add_action( 'admin_notices', 'wpcmrf_needed_notice' );

        deactivate_plugins( plugin_basename( __FILE__ ) ); 

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }
}
add_action( 'admin_init', 'has_wpcmrf_check' );

function wpcmrf_needed_notice(){
	global $plugin_name;
    ?><div class="error"><p><?php echo sprintf(esc_html__('The plugin %1$s requires the active plugin %2$s to be activated.','wpcmrf-lda'),'<strong>'.$plugin_name.'</strong>','<a href="https://github.com/CiviMRF/civimcrestface-wordpress">Connector to CiviCRM with CiviMcRestFace</a>'); ?></p></div><?php
}

function wpcmrf_db_failure_notice(){
    ?><div class="error"><p><?php echo sprintf(esc_html__('ERROR: Could not create database tables.','wpcmrf-lda'),'<strong>'.$plugin_name.'</strong>','<a href="https://github.com/CiviMRF/civimcrestface-wordpress">Connector to CiviCRM with CiviMcRestFace</a>'); ?></p></div><?php
}

/**
 * WPCMRF is not installing databases in all network WP databases - so this is done here
 */
function wpcmrf_database_adder() {
	if( is_plugin_active( 'connector-civicrm-mcrestface/wpcmrf.php' ) ) {
		global $wpdb;
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$table_name = $wpdb->prefix . "wpcivimrf_profile";
		$charset_collate = $wpdb->get_charset_collate();
		$sql = "CREATE TABLE $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			label varchar(255) DEFAULT '' NOT NULL,
			connector varchar(255) DEFAULT '' NOT NULL,
			url varchar(255) DEFAULT '' NOT NULL,
			site_key varchar(255) DEFAULT '' NOT NULL,
			api_key varchar(255) DEFAULT '' NOT NULL,      
		PRIMARY KEY  (id)
		) ENGINE = InnoDB $charset_collate;";

		dbDelta( $sql );

		$table_name = $wpdb->prefix . "wpcmrf_core_call";
		$sql = "CREATE TABLE `$table_name` (
			`cid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Call ID',
			`status` varchar(8) NOT NULL DEFAULT 'INIT' COMMENT 'Status',
			`connector_id` varchar(255) NOT NULL DEFAULT '' COMMENT 'Connector ID',
			`request` longtext COMMENT 'The request data sent',
			`reply` longtext COMMENT 'The reply data received',
			`metadata` text COMMENT 'Custom metadata on the request',
			`request_hash` varchar(255) NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the request, enables quick lookups for caches',
			`create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Creation timestamp of this call',
			`scheduled_date` timestamp NULL DEFAULT NULL COMMENT 'Scheduted timestamp of this call',
			`reply_date` timestamp NULL DEFAULT NULL COMMENT 'Reply timestamp of this call',
			`cached_until` timestamp NULL DEFAULT NULL COMMENT 'Cache timeout of this call',
			`retry_count` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Retry counter for multiple submissions',  
			PRIMARY KEY (`cid`),
			KEY `cmrf_by_connector` (`connector_id`,`status`),
			KEY `cmrf_cache_index` (`connector_id`,`request_hash`,`cached_until`)
		) ENGINE = InnoDB $charset_collate";

		dbDelta( $sql );
	}
	else {
		add_action( 'admin_notices', 'wpcmrf_db_failure_notice' );
	}
}
register_activation_hook( __FILE__, 'wpcmrf_database_adder' );
